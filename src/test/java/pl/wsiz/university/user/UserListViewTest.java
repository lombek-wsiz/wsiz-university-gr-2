package pl.wsiz.university.user;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserListViewTest {

    @Test
    void withSpaces() {
        UserListView userListView = new UserListView(null);

        String textWithSpaces = userListView.withSpaces("Andrzej", 20);

        assertEquals("Andrzej             ", textWithSpaces);
    }

    @Test
    void withSpaces2() {
        UserListView userListView = new UserListView(null);

        String textWithSpaces = userListView.withSpaces("Kowalski", 15);

        assertEquals("Kowalski       ", textWithSpaces);
    }

}
package pl.wsiz.university;

import org.junit.jupiter.api.Test;
import pl.wsiz.university.administrator.Administrator;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdministratorTest {

    @Test
    void getFirstName() {
        Administrator administrator = new Administrator("Adam", "Nowak",
                "adamk@onet.eu", "adam@52",
                LocalDate.of(1990, 6, 25));

        String firstName = administrator.getFirstName();

        assertEquals("Adam", firstName);
    }

}
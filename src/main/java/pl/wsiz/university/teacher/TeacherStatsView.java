package pl.wsiz.university.teacher;

import pl.wsiz.university.user.UserRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TeacherStatsView {

    private UserRepository userRepository;

    public TeacherStatsView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initialize() {
        System.out.println("Tytuł naukowy\tLiczba nauczycieli");
        Map<String, Integer> map = new HashMap<>();

        // zasilenie na sztywno, TODO: do poprawy na dynamiczne wyliczanie na podstawie userRepository
        map.put("dr", 32);
        map.put("dr hab.", 6);
        map.put("mgr", 8);
        map.put("mgr inż.", 12);

        Set<String> academicDegrees = map.keySet();
        for (String academicDegree: academicDegrees) {
            Integer counters = map.get(academicDegree);
            System.out.println(academicDegree+"\t"+counters);
        }
    }

}

package pl.wsiz.university.administrator;

import pl.wsiz.university.student.StudentAddView;
import pl.wsiz.university.teacher.TeacherAddView;
import pl.wsiz.university.teacher.TeacherStatsView;
import pl.wsiz.university.user.UserListView;
import pl.wsiz.university.user.UserRepository;

import java.util.Scanner;

public class AdministratorMenuView {

    private UserRepository userRepository;

    public AdministratorMenuView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initialize() {
        int chosen;
        do {
            System.out.println("====== MENU ADMINISTRATORA =====");
            for (AdministratorMenuItem item: AdministratorMenuItem.values()) {
                System.out.println(item.getNr()+" - "+item.getDescription());
            }
            System.out.println("Co wybierasz? ");
            Scanner scanner = new Scanner(System.in);
            chosen = scanner.nextInt();

            // 1 przykład rozwiązania
            /* if (chosen == AdministratorMenuItem.USER_LIST.getNr()) {
                new UserListView(userRepository).initialize();
            } else if (chosen == AdministratorMenuItem.STUDENT_ADD.getNr()) {
                new StudentAddView(userRepository).initialize();
            } else if (chosen == AdministratorMenuItem.TEACHER_ADD.getNr()) {
                new TeacherAddView(userRepository).initialize();
            } */

            AdministratorMenuItem chosenEnum = AdministratorMenuItem.convert(chosen);

            // 2 przykład rozwiązania (ze switch)
            switch (chosenEnum) {
                case USER_LIST:
                    new UserListView(userRepository).initialize();
                    break;
                case STUDENT_ADD:
                    new StudentAddView(userRepository).initialize();
                    break;
                case TEACHER_ADD:
                    new TeacherAddView(userRepository).initialize();
                    break;
                case ADMINISTRATOR_ADD:
                    new AdministratorAddView(userRepository).initialize();
                    break;
                case TEACHER_STATS:
                    new TeacherStatsView(userRepository).initialize();
                    break;
            }

        } while (chosen != AdministratorMenuItem.EXIT.getNr());
    }

}

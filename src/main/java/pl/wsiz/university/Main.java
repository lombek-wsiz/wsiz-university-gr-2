package pl.wsiz.university;

import pl.wsiz.university.administrator.Administrator;
import pl.wsiz.university.administrator.AdministratorMenuView;
import pl.wsiz.university.student.Student;
import pl.wsiz.university.teacher.Teacher;
import pl.wsiz.university.user.FileUserRepository;
import pl.wsiz.university.user.LoginView;
import pl.wsiz.university.user.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Adam", "Nowak",
                "adamk@onet.eu", "adam@52",
                LocalDate.of(1990, 6, 25), 95152);
        Teacher teacher1 = new Teacher("Iwona", "Nowak",
                "iwona@onet.eu", "Nowak%2@52",
                LocalDate.of(1994, 2, 12), "dr inż.");
        Administrator administrator1 = new Administrator("Jan", "Kowalski",
                "janek@gmail.com", "Jan98$",
                LocalDate.of(1982, 4, 30));

        FileUserRepository fileUserRepository = new FileUserRepository();
        fileUserRepository.insert(student1);
        fileUserRepository.insert(teacher1);
        fileUserRepository.insert(administrator1);

        LoginView loginView = new LoginView(fileUserRepository);
        User loggedUser = loginView.login();

        System.out.println("Zalogowano jako: ");
        printUser(loggedUser);

        if (loggedUser instanceof Administrator) {
            new AdministratorMenuView(fileUserRepository).initialize();
        }
    }

    private static void printUser(User user) {
        System.out.println("Imię i nazwisko: "+user.getFirstName()+" "+user.getLastName());
        System.out.println("Adres email: "+user.getEmail());
        System.out.println("Data urodzenia: "+user.getDateOfBirth()
                .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));

        // 1 przykład rozwiązania
        /*
        if (user instanceof Administrator) {
            System.out.println("Rola: Administrator");
        } else if (user instanceof Teacher) {
            System.out.println("Rola: Nauczyciel");
        } else if (user instanceof Student) {
            System.out.println("Rola: Student");
        }
         */

        // 2 przykład rozwiązania
        System.out.println("Rola: "+user.getRole());
    }

}